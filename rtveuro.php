<?php
/* rtveuroagd 3.0.2 */

ini_set('max_execution_time','60');
header('Content-type: text/plain; charset=utf-8; lang=pl');

$link="https://www.euro.com.pl/cms/it-super-okazje.bhtml";
$promomax=5; //ilosc promocji do wyswietlenia

include 'libshopcurl.php';

$promo=explode('<h3 class="product-name">',$content);
if(!isset($promo[1])){
	echo "Przykro mi, ale prawdopodobnie aktualnie zaden produkt nie jest w promocji. Sprawdz ponownie pozniej.";
	die();
}

echo 'Produkt dnia w RTV EURO AGD -> ',$link;

for($i=1;$i<=$promomax;$i++){
	if(!isset($promo[$i])){
		break;
	}
	
	$duplicate=FALSE;
	
	$desc=explode('<h4 class="product-group-name">',$promo[$i]);
	$oldcash=explode('<span class="product-old-price">',$promo[$i]);
	$newcash=explode('<strong class="product-price">',$promo[$i]);
	
	$promo[$i]=substr($promo[$i], 0, strpos($promo[$i], "</a>"));
	$promo[$i]=strip_tags($promo[$i]);
	$promo[$i]=str_replace("\r", "",$promo[$i]);
	$promo[$i]=str_replace("\n", "",$promo[$i]);
	$promo[$i]=str_replace("\t", "",$promo[$i]);
	$promo[$i]=str_replace("  ", "",$promo[$i]);
	
	//sprawdzanie dubli promocji
	for($j=1;$j<sizeof($promo);$j++){
		if($i == $j) {
			continue;
		}
		if($promo[$i] == $promo[$j]){
			$duplicate=TRUE;
			break;
		}
	}
	if($duplicate == TRUE){
		continue;
	}
	
	echo "\n==========\n";
	
	if(!isset($desc[1])){
		$desc="brak opisu";
	}
	else {
		$desc=substr($desc[1], 0, strpos($desc[1], "</h4>"));
		$desc=strip_tags($desc);
		$desc=str_replace("\r", "",$desc);
		$desc=str_replace("\n", "",$desc);
		$desc=str_replace("\t", "",$desc);
	}
	
	echo $i,". ",$promo[$i],", ",$desc;

	if(!isset($oldcash[1])){
		$oldcash="brak danych";
	}
	else {
		$oldcash=substr($oldcash[1], 0, strpos($oldcash[1], '</span>'));
		$oldcash=str_replace("&nbsp;","",$oldcash);
		$oldcash=str_replace(" ","",$oldcash);
	}
	
	if(!isset($newcash[1])){
		$newcash="brak danych";
	}
	else {
		$newcash=substr($newcash[1], 0, strpos($newcash[1], '</strong>'));
		$newcash=str_replace("&nbsp;","",$newcash);
		$newcash=str_replace(" ","",$newcash);
	}
	
	
	if($oldcash == "brak danych" || $newcash == "brak danych"){
		echo "\nNowa cena: ",$newcash," | ","Stara cena: ",$oldcash;
	}
	else {
		$discount=$oldcash-$newcash;
		$procent=($discount/$oldcash)*100;
		$procent=round($procent, 1);
		echo "\nNowa cena: ",$newcash," | ","Stara cena: ",$oldcash," (obnizka: ",number_format($discount, 2, ',', ''),"zl",", -",$procent,"%)";
	}
}
if(sizeof($promo) > $promomax+1){
	echo "\nIlosc wyswietlonych promocji ograniczona do ",$promomax,". ",sizeof($promo)-$promomax-1," nie wyswietlone tutaj, z powodu tego limitu.";
}
?>
