<?php
/* morele 2.2.1 */

ini_set('max_execution_time','60');
header('Content-type: text/plain; charset=utf-8; lang=pl');

$link="https://morele.net";

include 'libshopcurl.php';

$promo=explode('<div class="promo-box-name">',$content);
if(!array_key_exists(1, $promo)){
        echo "Aktualnie nie ma zadnej promocji. Sprawdz ponownie pozniej.";
        die();
}

$promo=substr($promo[1], 0, strpos($promo[1], "</div>"));
$promo=strip_tags($promo);
$promo=str_replace("\r", "",$promo);
$promo=str_replace("\n", "",$promo);
$promo=str_replace("  ", "",$promo);

$oldcash=explode('<div class="promo-box-old-price--label"', $content);
$oldcash=substr($oldcash[1], 0, strpos($oldcash[1], 'zł'));
$oldcash=str_replace("Cena bez kodu", "",$oldcash);
$oldcash=str_replace("\n", "",$oldcash);
$oldcash=strip_tags($oldcash);
$oldcash=str_replace(" ","",$oldcash);
$oldcash=substr($oldcash, 1);

$newcash=explode('<div class="promo-box-new-price--label">', $content);
$coupon=substr($newcash[1], 0, strpos($newcash[1], '</div>'));
$newcash=substr($newcash[1], 0, strpos($newcash[1], 'zł'));
$newcash=str_replace($coupon, "",$newcash);
$newcash=strip_tags($newcash);
$newcash=str_replace("\n", "",$newcash);
$newcash=str_replace(" ","",$newcash);
$coupon=str_replace("Cena z kodem: ", "",$coupon);
$coupon=str_replace(" ","",$coupon);

$procentold=str_replace(" ","",$oldcash);
$procentold=str_replace("zł","",$procentold);
$procentnew=str_replace(" ","",$newcash);
$procentnew=str_replace("zł","",$procentnew);
$discount=$procentold-$procentnew;
$procent=($discount/$procentold)*100;
$procent=round($procent, 1);

$left=explode('<div class="status-box-was">',$content);
$left=substr($left[1], 0, strpos($left[1], '</div>'));
$left=strip_tags($left);
$left=str_replace("\r", "",$left);
$left=str_replace("\n", "",$left);

if($coupon != NULL){
        $coupon=" | Kupon: ".$coupon;
}

echo $promo," | Nowa cena: ",$newcash,"zł | Stara cena: ",$oldcash,"zł (obnizka: ",number_format($discount, 2, ',', ''),"zł",", -",$procent,"%)",$coupon, " | ",$left," | ",$link;
?>
