<?php
/* bash 1.1.2 */
$expectedpoints=4000; //Oczekiwana minimalna liczba punktow cytatu do wylosowania

/* ********** */
header('Content-type: text/plain; charset=utf-8');
$points[0]=0;
while($points[0]<$expectedpoints){
	$bash=file_get_contents('http://bash.org.pl/random/');
	$points=explode('<span class=" points">',$bash);
	$points=explode('</span>',$points[1]);
}
$answer=explode('<div class="quote post-content post-body">',$bash);
$answer=explode("</div>",$answer[1]);
$answer=str_replace("&quot;","'",$answer[0]);
$answer=str_replace("&lt;","<",$answer);
$answer=str_replace("&gt;",">",$answer);
$answer=str_replace("<br />","\n",$answer);
$answer=str_replace("<br/>","",$answer);
$answer=str_replace("<br>","",$answer);
$answer=str_replace("<BR>","\n",$answer);
$answer=str_replace("	","",$answer);
$answer=str_replace("\r", "",$answer);
$answer=str_replace("&#39;",'"',$answer);
echo $answer;
$numcytat=explode('<head>',$bash);
$numcytat=explode('<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />',$numcytat[1]);
$numcytat=str_replace("<title>","",$numcytat[0]);
$numcytat=str_replace("</title>","",$numcytat);
$numcytat=str_replace("	","",$numcytat);
$numcytat=str_replace("\r", "",$numcytat);
$numcytat=str_replace("\n", "",$numcytat);
echo "\n",$numcytat," | Ocena: ",$points[0];
?>