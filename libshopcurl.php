<?php
/* libshopcurl 1.1.1 */

/* Skrypty uzywajace aktualnie tej biblioteki:
   - morele
   - xkom
   - rtveuro
*/
$maxretries=3;
$timeout=15;

$useragentbase = array('Opera/9.80 (Windows NT 6.1; Win64; x64; Edition Next) Presto/2.12.388 Version/12.15',
					   'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
					   'Mozilla/5.0 (Linux; Android 7.1; Nexus 7 Build/NDE63P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.134 Safari/537.36',
					   'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36',
					   'Mozilla/5.0 (Linux; Android 4.4.4; LG-P760 Build/KTU84Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.0.0 Mobile Safari/537.36',
					   'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.92 Safari/537.36)'
					  );
$useragent=$useragentbase[rand(0,sizeof($useragentbase)-1)];
$dir            = dirname(__FILE__);
$cookie_file    = $dir . '/cookies/' . md5($_SERVER['REMOTE_ADDR']) . '.txt';
$ch = curl_init($link);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_file);
curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file);
curl_setopt($ch, CURLOPT_URL, $link);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
curl_setopt($ch, CURLOPT_AUTOREFERER, true );
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout );
curl_setopt($ch, CURLOPT_TIMEOUT, $timeout );
curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
for($i=0;$i<$maxretries;$i++) {
	try {
		$content = curl_exec($ch);
	}
	catch (Exception $e) {
		echo 'Wystapill wyjatek nr '.$e->getCode().', jego komunikat to:'.$e->getMessage();
		die();
	}
	if($errno = curl_errno($ch)) {
		continue;
	}
	break;
}
if($errno = curl_errno($ch)) {
	echo "Nie udalo sie pobrac danych. Blad: ".$errno.". Probowalem ".$i." razy.";
	die();
}
curl_close($ch);
?>
