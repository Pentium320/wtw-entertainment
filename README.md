## wtw-entertainment

Jest to zbiór losowych skryptów parsujących, dla IRC'owego bota na #wtw @ Quakenet.

## Aktualne skrypty

- bash: skrypt zwracający losowy cytat z bash.org.pl.
	- zwraca cytat, ocenę oraz numer cytatu
	- konfigurowalna minimalna ocena cytatu (domyślnie 4000)

- libshopcurl: skrypt "biblioteka", bazujący na libcurl, który jest wymagany do większości skryptów do połączenia.
	- konfigurowalna liczba prób połączeń (domyślnie 3)
	- konfigurowalny czas oczekiwania (domyślnie 15 sekund)
	- obsługa błędów połączeń i wyjątków: https://curl.haxx.se/libcurl/c/libcurl-errors.html

- libshop: stary skrypt "biblioteka", o podobnym działaniu co libshopcurl, z tym, że używa funkcji "file_get_contents". Nieaktualizowany, nie zalecane używanie bez konkretniejszych powodów.
	- konfigurowalna liczba prób połączeń

- morele: skrypt zwracający promocję "w samo południe" na morele.net.
	- zwracana jest promocja, opis, nowa cena, stara cena, obniżka, procentowa obniżka i ilość sztuk do końca.
	* skrypt używa do działania libshopcurl. libshop nieobsługiwany.

- rtveuro: skrypt zwracający promocje w rtveuroagd.
	- zwracana jest promocja, opis, nowa cena, stara cena, obniżka oraz procentowa obniżka.
	- konfigurowalna ilość zwracanych promocji (domyślnie maksymalnie 5)
	* skrypt używa do działania libshopcurl (można także użyć starszej libshop)

- satysfakcja: skrypt zwracający promocję "gorący strzał" na satysfakcja.pl
	- zwracana jest promocja, nowa cena, stara cena, obniżka, procentowa obniżka, ilość sztuk do końca oraz średnia ocena.
	* skrypt używa do działania libshop

- xkom: skrypt zwracający promocję "gorący strzał" w xkom.pl
	- zwracana jest promocja, nowa cena, stara cena, obniżka, procentowa obniżka oraz ilość sztuk w promocji
	* skrypt używa do działania libshopcurl (można także użyć starszej libshop)
