<?php
/* x-kom 3.1.0 */

ini_set('max_execution_time','60');
header('Content-type: text/plain; charset=utf-8; lang=pl');

$link="https://x-kom.pl/goracy_strzal";

include 'libshopcurl.php';

$errornopromo = "Nie ma aktualnie zadnej promocji (ewentualnie skrypt wywinal orla). Sprawdz ponownie pozniej.";
$promo_errored = 0;
$promo=explode('{"hotShot":[{"type":"HotShot","extend":{',$content);
if(!isset($promo[1])) {
	echo $errornopromo;
	die();
}
else{
	$promo=explode('"hotShotBuyOffers"',$promo[1]);
}

$promo=str_replace('"',"",$promo[0]);
$promo=explode(',',$promo);

$i=0;
while (array_key_exists($i, $promo)) {
	//id promocji
	if (!isset($id) || str_contains($promo[$i], "id:")) {
		$id=str_replace("id:","",$promo[$i]);
	}

	//nowa cena
    elseif (!isset($newcash) || str_contains($promo[$i], "price:")) {
        $newcash=str_replace("price:","",$promo[$i]);
    }

	//stara cena
	elseif (!isset($oldcash) || str_contains($promo[$i], "oldPrice:")) {
		$oldcash=str_replace("oldPrice:","",$promo[$i]);
	}

	//tytul
	elseif (!isset($title) || str_contains($promo[$i], "promotionName:")) {
        $title=str_replace("promotionName:","",$promo[$i]);
    }
	
	//ile sztuk w promocji
	elseif (!isset($itemtotal) || str_contains($promo[$i], "promotionTotalCount:")) {
        $itemtotal=str_replace("promotionTotalCount:","",$promo[$i]);
    }
	
	//ile sztuk sprzedano
	elseif (!isset($itemsold) || str_contains($promo[$i], "saleCount:")) {
        $itemsold=str_replace("saleCount:","",$promo[$i]);
	}
	$i++;
}

//ile sztuk pozostalo w promocji
$itemleft=$itemtotal-$itemsold;

//kalkulowanie procentowej obnizki
if (isset($oldcash) || isset($newcash)) { 
	$procentold=str_replace(",","",$oldcash);
	$procentnew=str_replace(",","",$newcash);
	$discount=$procentold-$procentnew;
	$procent=$discount/$procentold*100;
	$procent=round($procent, 1);
}
else {
	$procent="N/A";
}

//tadam
echo "Goracy strzal #",$id, " - ",$title," | Nowa cena: ",$newcash,"zl | Stara cena: ",$oldcash,"zl (obnizka: ",$discount,"zl, -",$procent,"%) | W promocji pozostalo: ",$itemleft," na ",$itemtotal," sztuk | ",$link;
?>
